<%@page import="usuario.Usuario"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title> Sistema de Hotelaria </title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
	<style type="text/css">@import url("content/css/main.css"); </style>
	<style type="text/css">@import url("content/vendor/bootstrap/css/bootstrap.min.css");</style>
	<style type="text/css">@import url("content/fonts/font-awesome-4.7.0/css/font-awesome.min.css");</style>
	<style type="text/css">@import url("content/fonts/iconic/css/material-design-iconic-font.min.css");</style>
	<style type="text/css">@import url("content/vendor/bootstrap/css/bootstrap.min.css");</style>
	<style type="text/css">@import url("content/vendor/animate/animate.css"); </style>
	<style type="text/css">@import url("content/vendor/css-hamburgers/hamburgers.min.css");</style>
	<style type="text/css">@import url("content/vendor/animsition/css/animsition.min.css");</style>
	<style type="text/css">@import url("content/vendor/select2/select2.min.css");</style>
	<style type="text/css">@import url("content/vendor/daterangepicker/daterangepicker.css");</style>
	<style type="text/css">@import url("content/css/util.css");</style>
	<style type="text/css">@import url("content/css/main.css");</style>
	<style type="image/png">@import url("content/images/icons/favicon.ico");</style>

</head>

<body>
  <nav class="navbar navbar-expand-lg bg-secondary text-uppercase fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="Home.jsp">Home</a>
      <button class="navbar-toggler navbar-toggler-right text-uppercase font-weight-bold bg-primary text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item mx-0 mx-lg-1">
            <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="CadastrarHospede.jsp"> Cadastrar Hospede </a>
          </li>
          <li class="nav-item mx-0 mx-lg-1">
            <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="ReservarHospedar.jsp"> Reservar/Hospedar </a>
          </li>
          <li class="nav-item mx-0 mx-lg-1">
            <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="Consumo.jsp">Registrar Consumo</a>
          </li>
          <li class="nav-item mx-0 mx-lg-1">
            <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="Acomodacao.jsp">Cadastrar acomodação</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  
  	<div class="limiter">
		<div class="container-login100" style="background-image: url('content/images/bg-01.jpg');">	
		
		</div>
	</div>
</body>
</html>