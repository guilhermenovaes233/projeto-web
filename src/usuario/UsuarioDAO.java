package usuario;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import database.DBQuery;

public class UsuarioDAO extends DBQuery {
	
	private  Usuario usuario = null;
	
	public UsuarioDAO(Usuario usuario) {
		this.setTable	("usuario");
		this.setFields	(new String[]{"idUsuario","login","pass","email","ativo","obs"});
		this.setKeyField("idUsuario");
		this.setUsuario(usuario);
	}
	
	public UsuarioDAO() {
		this.setTable	("usuario");
		this.setFields	(new String[]{"idUsuario","login","pass","email","ativo","obs"});
		this.setKeyField("idUsuario");
	}
	
	public  ArrayList<Usuario> listByLogin(String login) {
		
		ArrayList<Usuario> tempListUsuarios = new ArrayList<Usuario>();
		try {
			ResultSet rs = select("login = "+login);
			while (rs.next()) {
				Usuario tempUser = new Usuario();
				tempUser.setIdUsuario( rs.getInt("idUsuario"));
				tempUser.setLogin( rs.getString("login") );
				tempUser.setPass ( rs.getString("senha") );
				tempUser.setEmail( rs.getString("email") );	
				tempListUsuarios.add(tempUser);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ( tempListUsuarios );
	}
	
	public  ArrayList<Usuario> listByEmail(String email) {
		
		ArrayList<Usuario> tempListUsuarios = new ArrayList<Usuario>();
		try {
			ResultSet rs = select("email = "+email);
			while (rs.next()) {
				Usuario tempUser = new Usuario();
				tempUser.setIdUsuario( rs.getInt("idUsuario"));
				tempUser.setLogin( rs.getString("login") );
				tempUser.setPass ( rs.getString("senha") );
				tempUser.setEmail( rs.getString("email") );	
				tempListUsuarios.add(tempUser);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ( tempListUsuarios );
	}

	public ArrayList<Usuario> listAll() {
		ArrayList<Usuario> tempListUsuarios = new ArrayList<Usuario>();
		try {
			ResultSet rs = select();
			while (rs.next()) {
				Usuario tempUser = new Usuario();
				tempUser.setIdUsuario( rs.getInt("idUsuario"));
				tempUser.setLogin( rs.getString("login") );
				tempUser.setPass ( rs.getString("pass") );
				tempUser.setEmail( rs.getString("email") );	
				tempListUsuarios.add(tempUser);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ( tempListUsuarios );		
	}
	
	public boolean checkLogin(String user, String pass){
		try {
			ResultSet rs = select("login = '"+ user +"' AND pass = '"+pass+"'");
			return (rs.next());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ( false );
	}
	
	public boolean checkEmail(String email){
		try {
			ResultSet rs = select("email = '"+ email + "'");
			
			return (rs.next());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ( false );
	}
	
	public String retornaPass(String email){
		String pass = "";
		try {
			ResultSet rs = select("email = '"+ email + "'");
			
			while(rs.next()) {
				pass = rs.getString("pass");
			};
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ( pass );
	}
	
	public void save() {
		if ( this.getUsuario().getIdUsuario() <= 0) {
			insert(this.getUsuario().toArray());
		}else {
			update(this.getUsuario().toArray());
		}
	}
	
	public void trash() {
		// delete( this.getUsuario().getIdUsuario() );
		this.getUsuario().setAtivo("N");
		update(this.getUsuario().toArray());
		
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}
