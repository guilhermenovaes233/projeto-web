package usuario;

import java.util.ArrayList;


public class Usuario {
	
	private int    idUsuario;
	private String login;
	private String pass;
	private String email;
	private String ativo;
	private String obs;
	
	public Usuario() {
	
	}
	
	public Usuario(String idUsuario, String login, String pass, String email, String ativo, String obs) {
		this.idUsuario =  ((idUsuario=="") ? 0 : new Integer(idUsuario));
		this.login		= login;
		this.pass		= pass;
		this.email		= email;
		this.ativo		= ativo;
		this.obs		= obs;
	}
	
	public Usuario(String idUsuario, String login, String pass, String email) {
		this.idUsuario =  ((idUsuario=="") ? 0 : new Integer(idUsuario));
		this.login		= login;
		this.pass		= pass;
		this.email		= email;
		this.ativo		= "s";
		this.obs		= "";
	}
	
	public Usuario( String login, String pass, String email) {
		this.login		= login;
		this.pass		= pass;
		this.email		= email;
	}
	
	public void save() {
		UsuarioDAO usuarioDAO = new UsuarioDAO( this );
		usuarioDAO.save();
	}
	
	public void delete() {
		UsuarioDAO usuarioDAO = new UsuarioDAO( this );
		usuarioDAO.delete(this.getIdUsuario());
	}
	
	public ArrayList<Usuario> listAll() {
		UsuarioDAO usuarioDAO = new UsuarioDAO( this );
		return ( usuarioDAO.listAll());
	}
	
	public ArrayList<Usuario> listByEmail(String email) {
		UsuarioDAO usuarioDAO = new UsuarioDAO( this );
		return ( usuarioDAO.listByEmail(email));
	}
	
	public boolean checkLogin() {
		UsuarioDAO usuarioDAO = new UsuarioDAO();
		return ( usuarioDAO.checkLogin( this.getLogin(), this.getPass()));	
	}
	
	public boolean checkEmail() {
		UsuarioDAO usuarioDAO = new UsuarioDAO();
		return ( usuarioDAO.checkEmail(this.getEmail()));	
	}
	
	public String checkPass(String email) {
		UsuarioDAO usuarioDAO = new UsuarioDAO(this);
		return ( usuarioDAO.retornaPass(email));	
	}
	
	public String[] toArray() {
		return(
		new String[] { 
				new Integer(  this.getIdUsuario() ).toString(), 
				this.getLogin(), 
				this.getPass(), 
				this.getEmail(), 
				this.getAtivo(), 
				this.getObs()  
		});
	}
	
	
	public String toString() {
		return(
				new Integer(  this.getIdUsuario() ).toString()+ 
				this.getLogin()+ 
				this.getPass()+
				this.getEmail()+ 
				this.getAtivo()+ 
				this.getObs()  
		);
	}
	
	public int getIdUsuario() {
		return idUsuario;
	}
	
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	
	public String getLogin() {
		return login;
	}
	
	public void setLogin(String login) {
		this.login = login;
	}
	
	public String getPass() {
		return pass;
	}
	
	public void setPass(String pass) {
		this.pass = pass;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getAtivo() {
		return ativo;
	}
	
	public void setAtivo(String ativo) {
		this.ativo = ativo;
	}
	
	public String getObs() {
		return obs;
	}
	
	public void setObs(String obs) {
		this.obs = obs;
	}
	
}
