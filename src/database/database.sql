

CREATE DATABASE pwd CHARSET utf8;

use pwd;

CREATE TABLE usuario (
  idUsuario int(11) NOT NULL AUTO_INCREMENT,
  login varchar(32) NOT NULL,
  pass varchar(32) NOT NULL,
  email varchar(100) NOT NULL,
  ativo char(1) DEFAULT 'S',
  obs varchar(255) DEFAULT NULL,
  PRIMARY KEY (idUsuario)
);

insert into usuario(idUsuario, login, pass, email, ativo)
values (1, 'admin', 'admin', 'admin@admin.com', 'S')



CREATE TABLE hospede (
    idHospede INT AUTO_INCREMENT PRIMARY KEY,
    email VARCHAR(255) NOT NULL,
    DtNascimento DATE,
    cpf varchar(100) NOT NULL
);

insert into hospede(idHospede, email, DtNascimento, cpf)
values (1, 'teste@teste.com', '2008-7-04', '00000000025')





