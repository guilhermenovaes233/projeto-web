package hospede;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import database.DBQuery;

public class HospedeDAO extends DBQuery {
	
	private  Hospede hospede = null;
	
	public HospedeDAO(Hospede hospede) {
		this.setTable	("hospede");
		this.setFields	(new String[]{"idHospede","email","DtNascimento","cpf"});
		this.setKeyField("idHospede");
		this.setHospede(hospede);
	}

	public HospedeDAO() {
		this.setTable	("hospede");
		this.setFields	(new String[]{"idHospede","email","DtNascimento","cpf"});
		this.setKeyField("idHospede");
	}
	
	public  ArrayList<Hospede> listByHospedes(int IdHospede) {
		
		ArrayList<Hospede> tempListHospede = new ArrayList<Hospede>();
		try {
			ResultSet rs = select("idHospede = "+IdHospede);
			
			while (rs.next()) {
				Hospede tempUser = new Hospede();
				tempUser.setIdHospede( rs.getInt("IdHospede"));
				tempUser.setEmail( rs.getString("email"));	
				tempUser.setDtNascimento(rs.getString("dtNascimento"));
				tempUser.setCPF(rs.getString("cpf"));
				
				tempListHospede.add(tempUser);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return (tempListHospede);
	}
	
	public  ArrayList<Hospede> listByEmail(String email) {
		ArrayList<Hospede> tempListHospede = new ArrayList<Hospede>();
		try {
			ResultSet rs = select("email = "+email);
			while (rs.next()) {
				Hospede tempUser = new Hospede();
				tempUser.setIdHospede( rs.getInt("IdHospede"));
				tempUser.setEmail( rs.getString("email"));	
				tempUser.setDtNascimento(rs.getString("dtNascimento"));
				tempUser.setCPF(rs.getString("cpf"));
				
				tempListHospede.add(tempUser);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ( tempListHospede );
	}
	
	public ArrayList<Hospede> listAll() {
		ArrayList<Hospede> tempListHospede = new ArrayList<Hospede>();
		try {
			ResultSet rs = select();
			while (rs.next()) {
				Hospede tempUser = new Hospede();
				tempUser.setIdHospede( rs.getInt("IdHospede"));
				tempUser.setEmail( rs.getString("email"));	
				tempUser.setDtNascimento(rs.getString("dtNascimento"));
				tempUser.setCPF(rs.getString("cpf"));
				
				tempListHospede.add(tempUser);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ( tempListHospede );		
	}
	
	public boolean checkHospede(int IdHospede){
		try {
			ResultSet rs = select("login = '"+ IdHospede);
			return (rs.next());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ( false );
	}
	
	public boolean checkEmail(String email){
		try {
			ResultSet rs = select("email = '"+ email + "'");
			
			return (rs.next());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ( false );
	}
	
	public boolean checkCPF(String cpf){
		try {
			ResultSet rs = select("cpf = '"+ cpf + "'");
			
			return (rs.next());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ( false );
	}
	
	public boolean checkDtNascimento(String dtNascimento){
		try {
			ResultSet rs = select("DtNascimento = '"+ dtNascimento);
			return (rs.next());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ( false );
	}
	
	public void save() {
		if ( this.getHospede().getIdHospede() <= 0) {
			insert(this.getHospede().toArray());
		}else {
			update(this.getHospede().toArray());
		}
	}
	
	public void delete() {
		if ( this.getHospede().getIdHospede() > 0) {
			delete( this.getHospede().getIdHospede() );
		}
	}
	
	public void alter() {
		if ( this.getHospede().getIdHospede() <= 0) {
			insert(this.getHospede().toArray());
		}else {
			update(this.getHospede().toArray());
		}
	}

	public Hospede getHospede() {
		return hospede;
	}

	public void setHospede(Hospede hospede) {
		this.hospede = hospede;
	}
}
